package com.uadb.demospring.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.web.bind.annotation.CrossOrigin;
@RepositoryRestResource
@CrossOrigin("*")
public interface ProductRep extends JpaRepository<Product, Long> {

	@RestResource(path="/produitDispo")
	public List<Product> findByDisponiblityIsTrue();
	
	@RestResource(path="/produitPromotion")
	public List<Product> findByPromotionIsTrue();
	
	@RestResource(path="/produitSelected")
	public List<Product> findBySelectedIsTrue();
	 
}
