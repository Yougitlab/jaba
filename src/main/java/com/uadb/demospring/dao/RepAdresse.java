package com.uadb.demospring.dao;

import org.springframework.data.jpa.repository.JpaRepository;

public interface RepAdresse extends JpaRepository<Adresse, Long> {

}
