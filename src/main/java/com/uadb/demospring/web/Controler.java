package com.uadb.demospring.web;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.uadb.demospring.dao.Categorie;
import com.uadb.demospring.dao.CategorieRep;
import com.uadb.demospring.dao.Commande;
import com.uadb.demospring.dao.CommandeRep;
import com.uadb.demospring.dao.Product;
import com.uadb.demospring.dao.ProductItem;
import com.uadb.demospring.dao.ProductRep;
import com.uadb.demospring.dao.ProduitItemRep;
import com.uadb.demospring.dao.Utilisateur;
import com.uadb.demospring.dao.UtilisateurRep;

@CrossOrigin("*")
@RestController
public class Controler {
	@Autowired
	private CategorieRep categorieRep;
	@Autowired
	private ProductRep productRep;
	@Autowired
	RepositoryRestConfiguration repositoryRestConfig;
	@Autowired
	CommandeRep commandeRp;
	@Autowired
	UtilisateurRep utilisateurRep;
	@Autowired
	ProduitItemRep produitItemRep;
	
	@RequestMapping(method = RequestMethod.POST,path="/commander")
	public Commande SaveCommandeNoUser(@RequestBody Commande commande) {
		Utilisateur client=commande.getUtilisateur();
		//ajout adresse reste
		client.setDate(new Date());
		client=utilisateurRep.getOne(client.getId());
		
		Commande cm=new Commande();
		cm.setDate(new Date());
		cm.setUtilisateur(client);
		cm.setDate(new Date());
		cm.setTotalOrder(commande.getTotalOrder());
		cm=commandeRp.save(cm);
		
		for(ProductItem pi:commande.getProducts()) {
				Product pr=productRep.getOne(pi.getProduct().getId());
				System.out.println(pi.getProduct().getId());
				System.out.println(pr.getId());
				System.out.println(pr.getDesignation());
				if(pr!=null) {
					pi.setId(null);
					pi.setCommande(cm);
					pi.setProduct(pr);
					pi.setDate(new Date());
					produitItemRep.save(pi);
				}
		}
		return cm;
	}
	@RequestMapping(method = RequestMethod.POST,path="/myOrder")
	public Commande SaveCommande(@RequestBody Commande commande) {
		Utilisateur client=commande.getUtilisateur();
		//ajout adresse reste
		client.setDate(new Date());
		client=utilisateurRep.save(client);
		Commande cm=new Commande();
		cm.setDate(new Date());
		cm.setUtilisateur(client);
		cm.setDate(new Date());
		cm.setTotalOrder(commande.getTotalOrder());
		cm=commandeRp.save(cm);
		
		for(ProductItem pi:commande.getProducts()) {
				Product pr=productRep.getOne(pi.getProduct().getId());
				System.out.println(pi.getProduct().getId());
				System.out.println(pr.getId());
				System.out.println(pr.getDesignation());
				if(pr!=null) {
					pi.setId(null);
					pi.setCommande(cm);
					pi.setProduct(pr);
					pi.setDate(new Date());
					produitItemRep.save(pi);
				}
		}
		
		return cm;
	}
	@GetMapping(path="/photo/{id}",produces=MediaType.IMAGE_JPEG_VALUE)
	public byte[]getPhotos(@PathVariable Long id) throws Exception{
		return Files.readAllBytes(Paths.get(System.getProperty("user.home")+"/diaba/photos/"+id+".jpg"));
		
	}
	@PostMapping(path="/uploadPhoto/{id}")
	public void uploadPhoto(MultipartFile file,@PathVariable Long id) throws Exception{
		Product p=productRep.getOne(id);
		
		Files.write(Paths.get(System.getProperty("user.home")+"/diaba/photos/"+p.getId()+".jpg"),file.getBytes());
		
	}
	@PostMapping(path="/addCategorie")
	public Categorie addCategorie(@RequestBody Categorie categorie) {
		categorie.setDate(new Date());
		return categorieRep.save(categorie);
	}
	@PostMapping(path="/addProduct")
	public Product addProduct(@RequestBody Product product) {
		product.setDate(new Date());
		
			
		return productRep.save(product) ;
	}
	@PostMapping(path="/addUser")
	public Utilisateur addUser(@RequestBody Utilisateur user) {
		user.setDate(new Date());
		
			
		return utilisateurRep.save(user) ;
	}
	@PutMapping(path="/upProduct")
	public Product upProduct(@RequestBody Product product) {
		product.setDate(new Date());
		return productRep.save(product);
	}
	@DeleteMapping(path="/deleteProduct/{id}")
	void deleProduct(@PathVariable Long id) {
		
		productRep.deleteById(id);
	}

}
