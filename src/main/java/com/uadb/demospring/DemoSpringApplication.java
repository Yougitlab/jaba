package com.uadb.demospring;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;

import com.uadb.demospring.dao.Categorie;
import com.uadb.demospring.dao.CategorieRep;
import com.uadb.demospring.dao.Commande;
import com.uadb.demospring.dao.Product;
import com.uadb.demospring.dao.ProductRep;
import com.uadb.demospring.dao.Utilisateur;
import com.uadb.demospring.dao.UtilisateurRep;

@SpringBootApplication
public class DemoSpringApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(DemoSpringApplication.class, args);
	}
	@Autowired
	private CategorieRep categorieRep;
	@Autowired
	private ProductRep productRep;
	@Autowired
	UtilisateurRep utilisateurRep;
	@Autowired
	RepositoryRestConfiguration repositoryRestConfig;

	@Override
	public void run(String... args) throws Exception {
		// TODO Auto-generated method stub
		repositoryRestConfig.exposeIdsFor(Categorie.class,Product.class,com.uadb.demospring.dao.ProductItem.class,Commande.class,Utilisateur.class);
		Categorie c=new Categorie(null,new Date(),"cereale","prduit du vallé",null);
		Categorie c1=new Categorie(null,new Date(),"Epice","100% naturelle",null);
		Categorie c2=new Categorie(null,new Date(),"Legume","prduit du vallé",null);
		categorieRep.save(c);
		categorieRep.save(c1);
		categorieRep.save(c2);
		productRep.save(new Product(null,new Date(),"Riz",false,false,true,1200,c,null));
		productRep.save(new Product(null,new Date(),"Sel",false,false,true,5000,c1,null));
		productRep.save(new Product(null,new Date(),"Carotte",false,true,true,100,c2,null));
		productRep.save(new Product(null,new Date(),"Mile",false,true,true,300,c,null));
		utilisateurRep.save(new Utilisateur(null,new Date(),"778022110","Cisse","Youssou","123","Client",null,null));
		
		
	}

}
